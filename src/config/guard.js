import { $store } from '../main';

module.exports = router => {
	router.beforeEach((to, from, next) => {
		var access_token = localStorage.getItem('access_token'),
			expires_at = localStorage.getItem('expires_at');

		// Get a new token if expired
		if (!access_token || new Date(parseInt(expires_at)) < new Date()) {
			$store.dispatch('oauth/getToken', {
				callback: () => next()
			});
		} else {
			next();
		}
	});
}
