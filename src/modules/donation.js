import Vue from 'vue';
import getHeaders from '../config/headers';

export default {
	namespaced: true,

	state: {
		step: 1,
		person: {
			first_name: '',
			last_name: '',
			email: '',
			phone: '',
			mobile: '',
			rg: '',
			cpf: ''
		},
		address: {
			street: '',
			complement: '',
			neighbourhood: '',
			number: '',
			zipcode: '',
			state: 'PB',
			country_id: '76'
		},
		donation: {
			value: '',
			institution_id: '1'
		},
		institution: {
			name: '',
			thumbnail: '',
			location: {
				lat: '',
				lng: ''
			}
		},
		redirect_uri: ''
	},

	getters: {
		getPerson(state) {
			return state.person;
		},
		getAddress(state) {
			return state.address;
		},
		getDonation(state) {
			return state.donation;
		},
		getStep(state) {
			return state.step;
		},
		getUri(state) {
			return state.redirect_uri;
		},
		getInstitution(state) {
			return state.institution;
		}
	},

	actions: {
		addDonation({ commit }, payload) {
			return new Promise((resolve, reject) => {
				Vue.http.post(`${process.env.API_URL}/donations`,
					payload,
					{ headers: getHeaders() }
				)
				.then(response => {
					if (response.status == 200) {
						commit('setUri', response.body.redirect_uri)

						return resolve(response.body);
					}

					return reject(response);
				}, error => console.error(error))
			})
		}
	},

	mutations: {
		setStep(state, payload) {
			state.step = payload;
		},
		setValue(state, payload) {
			state.donation.value = payload;
		},
		setUri(state, payload) {
			state.redirect_uri = payload;
		},
		setInstitution(state, payload) {
			state.institution = payload;
			state.donation.institution_id = payload.id;
		}
	}
}
