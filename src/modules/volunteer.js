import Vue from 'vue';
import getHeaders from '../config/headers';
import { $store } from '../main';

export default {
	namespaced: true,

	state: {
		step: 1,
		person: {
			first_name: '',
			last_name: '',
			email: '',
			phone: '',
			mobile: '',
			rg: '',
			cpf: ''
		},
		address: {
			street: '',
			complement: '',
			neighbourhood: '',
			number: '',
			zipcode: '',
			state: 'PB',
			country_id: '76'
		},
		volunteer: {
			civil_status: '',
			birthday: '',
			ocupation: '',
			religion: '',
			reason: '',
			suggestions: '',
			abilities: '',
			days: {
				sun: false,
				mon: true,
				tue: true,
				wed: true,
				thu: true,
				fri: true,
				sat: false
			},
			shifts: {
				first: false,
				second: true
			}
		}
	},

	getters: {
		getPerson(state) {
			return state.person;
		},
		getAddress(state) {
			return state.address;
		},
		getVolunteer(state) {
			return state.volunteer;
		},
		getStep(state) {
			return state.step;
		}
	},

	actions: {
		addVolunteer({ state, commit }, payload) {
			return new Promise((resolve, reject) => {
				Vue.http.post(`${process.env.API_URL}/volunteers`,
					{
						volunteer: Object.assign({}, payload.volunteer, {
							birthday: moment(payload.volunteer.birthday, 'DD/MM/YYYY').format('YYYY-MM-DD')
						}),
						person: state.person,
						address: state.address
					},
					{ headers: getHeaders() }
				)
				.then(response => {
					if (response.status == 200) {
						return resolve(response.body);
					}

					return reject(response);
				}, error => {
					if (error.status == 422) {
						let errors = error.body;
						let message = errors[Object.keys(errors)[0]][0]

						return $store.commit('logger/error', {
							message
						})
					}

					console.error(error);
				})
			})
		}
	},

	mutations: {
		setStep(state, payload) {
			state.step = payload;
		},
		setDay(state, payload) {
			state.volunteer.days[payload.key] = payload.value;
		},
		setShift(state, payload) {
			state.volunteer.shifts[payload.key] = payload.value;
		}
	}
}
