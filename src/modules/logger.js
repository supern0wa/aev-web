export default {
	namespaced: true,

	state: {
		history: [],
		queue: []
	},

	getters: {
		getHistory(state) {
			return state.history;
		},
		getQueue(state) {
			return state.queue;
		},
	},

	mutations: {
		error(state, payload) {
			state.queue.push({
				time: Date.now(),
				type: 'error',
				message: payload.message,
				theme: 'warn',
				timeout: payload.timeout ? payload.timeout : 5000
			})
		},
		warn(state, payload) {
			state.queue.push({
				time: Date.now(),
				type: 'warn',
				message: payload.message,
				theme: 'accent',
				timeout: payload.timeout ? payload.timeout : 5000
			})
		},
		info(state, payload) {
			state.queue.push({
				time: Date.now(),
				type: 'info',
				message: payload.message,
				theme: 'primary',
				timeout: payload.timeout ? payload.timeout : 5000
			})
		},
		pull(state, payload) {
			let index = state.queue.indexOf(payload.item);

			if (index != -1)
				state.history.push(state.queue.splice(index, 1));
		}
	}
}
