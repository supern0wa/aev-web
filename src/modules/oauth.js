import Vue from 'vue';

export default {
	namespaced: true,
	actions: {
		getToken({ state, commit }, payload) {
			return Vue.http.post(`${process.env.BASE_PATH}/authorize`, payload)
				.then(response => {
					commit('setSession', response.body);
					payload.callback();
				}, err => console.error(err));
		}
	},
	mutations: {
		setSession(state, payload) {
			let expires_at = payload.expires_in * 1000 + new Date().getTime();

			if (payload.refresh_token)
				localStorage.setItem('refresh_token', payload.refresh_token);

			localStorage.setItem('access_token', payload.access_token);
			localStorage.setItem('expires_at', expires_at);
		}
	}
}
