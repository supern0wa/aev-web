import Vue from 'vue';
import getHeaders from '../config/headers';

export default {
	namespaced: true,

	state: {
		//
	},

	getters: {
		//
	},

	actions: {
		getPage(context, payload) {
			return new Promise((resolve, reject) => {
				Vue.http.get(`${process.env.API_URL}/pages/${payload}`,
					{ headers: getHeaders() }
				)
				.then(response => {
					if (response.status == 200) {
						return resolve(response.body);
					}

					return reject(response);
				}, error => console.error(error));
			});
		},
	},

	mutations: {
		//
	}
}
