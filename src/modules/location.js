import Vue from 'vue';
import getHeaders from '../config/headers';

export default {
	namespaced: true,

	state: {
		countries: {},
		states: {
			PB: 'Paraíba',
			PE: 'Pernambuco'
		}
	},

	getters: {
		getCountries(state) {
			return state.countries;
		},

		getStates(state) {
			return state.states;
		}
	},

	actions: {
		fetchCountries({ state, commit }, payload) {
			return new Promise((resolve, reject) => {
				if (Object.keys(state.countries).length > 0)
					return resolve(state.countries)

				Vue.http.get(`${process.env.API_URL}/location/countries`, {
					headers: getHeaders()
				})
				.then(response => {
					if (response.status == 200) {
						response.body.forEach(country => {
							state.countries[country.id] = country.name;
						});

						return resolve(state.countries);
					}

					return reject(response);
				}, error => commit('logger/error', { message: 'Erro!' }));
			})
		},

		resolveCep(state, payload) {
			return Vue.http.get(`${process.env.API_URL}/location/cep?number=${payload}`, {
				headers: getHeaders()
			})
		}
	},

	mutations: {
		//
	}
}
