import Vue from 'vue';
import getHeaders from '../config/headers';

export default {
	namespaced: true,

	state: {
		posts: [],
		events: []
	},

	getters: {
		getPosts(state) {
			return state.posts;
		},
		getEvents(state) {
			return state.events;
		}
	},

	actions: {
		fetchPosts({ state }, payload) {
			return new Promise((resolve, reject) => {
				Vue.http.get(`${process.env.API_URL}/posts/search?q=${payload}`,
					{ headers: getHeaders() }
				)
				.then(response => {
					if (response.status == 200) {
						state.posts = response.body.data;

						return resolve(state.posts);
					}

					return reject(response);
				}, error => {
					console.error(error);
				})
			})
		},

		fetchEvents({ state }, payload) {
			return new Promise((resolve, reject) => {
				Vue.http.get(`${process.env.API_URL}/events/search?q=${payload}`,
					{ headers: getHeaders() }
				)
				.then(response => {
					if (response.status == 200) {
						state.events = response.body.data;

						return resolve(state.events);
					}

					return reject(response);
				}, error => {
					console.error(error);
				})
			})
		}
	},

	mutations: {
		//
	}
}
