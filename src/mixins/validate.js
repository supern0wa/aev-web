export default {
	methods: {
		onValidate () {
			this.$validator.validateAll()
				.then(success => {
					this.$parent.$emit('errors-changed', success);
				});
		}
	},

	created () {
		// Subscribes to parent's validate
		this.$parent.$on('validate', this.onValidate);
	},

	beforeDestroy () {
		this.$parent.$off('validate', this.onValidate);
	}
}
