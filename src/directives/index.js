import maskCpf from './mask/mask-cpf';
import maskPhone from './mask/mask-phone';
import maskCurrency from './mask/mask-currency';
import maskCep from './mask/mask-cep';
import maskDate from './mask/mask-date';

export default function install(Vue) {
	Vue.directive('ev-mask-cpf', maskCpf);
	Vue.directive('ev-mask-phone', maskPhone);
	Vue.directive('ev-mask-currency', maskCurrency);
	Vue.directive('ev-mask-cep', maskCep);
	Vue.directive('ev-mask-date', maskDate);
}
