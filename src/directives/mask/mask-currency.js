import VMasker from 'vanilla-masker';

export default {
	inserted (el, binding) {
		VMasker(el).maskMoney({
			precision: 2,
			separator: ',',
			delimiter: '.',
			unit: '',
			zeroCents: true
		});
	},

	unbind (el) {
		//
	}
}
