import donationRoutes from '../donation/routes';

export default [
	{
		path: '/search/:keyword',
		component: require('./Search'),
		children: [
			...donationRoutes
		]
	}
]
