import donationRoutes from '../donation/routes';
import volunteerRoutes from '../volunteer/routes';

export default [
	{
		path: '/about',
		component: require('./About'),
		children: [
			...donationRoutes,
			...volunteerRoutes
		]
	}
]

