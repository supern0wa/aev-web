import donationRoutes from '../donation/routes';
import volunteerRoutes from '../volunteer/routes';

export default [
	{
		path: '/blog',
		component: require('./Blog'),
		children: [
			...donationRoutes,
			...volunteerRoutes
		]
	},
	{
		path: '/blog/read/:id',
		component: require('./BlogRead'),
		children: [
			...donationRoutes,
			...volunteerRoutes
		]
	},
]
