import donationRoutes from '../donation/routes';
import volunteerRoutes from '../volunteer/routes';

export default [
	{
		path: '/events',
		component: require('./EventBrowse'),
		children: [
			...donationRoutes,
			...volunteerRoutes
		]
	},
	{
		path: '/events/read/:id',
		component: require('./EventRead')
	}
]
