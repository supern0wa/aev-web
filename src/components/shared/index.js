export default function install(Vue) {
	Vue.component('ev-button', require('./Button'));
	Vue.component('ev-slideshow', require('./Slideshow'));
	Vue.component('ev-map', require('./Map'));
	Vue.component('ev-text', require('./Text'));
	Vue.component('ev-textarea', require('./Textarea'));
	Vue.component('ev-select', require('./Select'));
	Vue.component('ev-gallery', require('./Gallery'));
	Vue.component('ev-gallery-fullscreen', require('./GalleryFullscreen'));
	Vue.component('ev-inner-header', require('./InnerHeader'));
	Vue.component('ev-content', require('./Content'));
	Vue.component('ev-modal', require('./Modal'));
	Vue.component('ev-stepper', require('./Stepper'));
	Vue.component('ev-cep-finder', require('./CepFinder'));
	Vue.component('ev-snackbar', require('./Snackbar'));
	Vue.component('ev-grid', require('./Grid'));
	Vue.component('ev-social-share', require('./SocialShare'));
	Vue.component('ev-form', require('./Form'));
}
