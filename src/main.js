import Vue from 'vue';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';
import Vuex from 'vuex';

import { router, store } from './config';

Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(Vuex);

export var $router = new VueRouter(router);
export var $store = new Vuex.Store(store);

// Initialize application
require('./bootstrap')($router, $store, require('./App'));
