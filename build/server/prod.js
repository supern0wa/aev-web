var app = require('../../server'),
	path = require('path'),
	history = require('connect-history-api-fallback');

app.use(history());

app.get('/*', function(req, res) {
	res.sendFile(path.join(__dirname, '..', '..', 'public', 'index.html'));
});

